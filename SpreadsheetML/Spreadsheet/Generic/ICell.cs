﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SageATL.Office.Spreadsheet.Generic
{
    public interface ICell
    {
        string GetValue();
        string GetReference();
        string GetFormula();
        void SetValue(string value);
        void SetFormula(string formula);

        string GetStyle();
        void SetStyle(string styleIndex);

    }
}

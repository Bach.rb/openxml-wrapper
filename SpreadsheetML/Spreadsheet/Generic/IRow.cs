﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SageATL.Office.Spreadsheet.Generic
{
    public interface IRow
    {
        List<ICell> GetCells();
        uint GetIndex();
        Nullable<double> GetRowHeight();
        void SetRowHeight(Nullable<double> height);
        void AddCell(string column);
        void AppendCell();
        void DeleteCell(); 
    }
}

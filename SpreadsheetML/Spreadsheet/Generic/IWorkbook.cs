﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SageATL.Office.Spreadsheet.Generic
{
    public interface IWorkbook
    {
        List<IWorksheet> GetSheets();

        void Open(MemoryStream stream);

        void Save();

        void SaveAs(Stream stream); 

        void Close();

        void AddSheet(uint index);
        void AppendSheet(); 
        void DeleteSheet(uint index); 

    }
}

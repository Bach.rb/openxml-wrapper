﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SageATL.Office.Spreadsheet.Generic
{
    public interface IWorksheet
    {
        List<IRow> GetRows();
        uint GetIndex();
        string GetName();
        void InsertRow(uint index);
        void AppendRow();
        void DeleteRow(uint index);

        void Save();

        List<IColumn> GetColumns();
        List<IImage> GetImages();
        void SetImages(List<IImage> images);
        void SetColumns(List<IColumn> columns);

        IWorksheetSettings GetWorksheetSettings();
        void SetWorksheetSettings(IWorksheetSettings value); 
    }
}

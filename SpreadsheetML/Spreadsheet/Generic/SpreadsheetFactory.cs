﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SageATL.Office.Spreadsheet.Generic
{
    public abstract class SpreadsheetFactory
    {
        public abstract IWorkbook CreateWorkbook();
        public abstract IWorksheet CreateWorksheet();
        public abstract IRow CreateRow();
        public abstract ICell CreateCell();
    }
}

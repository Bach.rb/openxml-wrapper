﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SageATL.Office.Spreadsheet.Generic
{
    public class GenericCell
    {
        private ICell _cell; 

        public string CellValue
        {
            get { return _cell.GetValue();  }
            set { _cell.SetValue(value); }
        }

        public string CellReference
        {
            get { return _cell.GetReference(); }
        }


        public string CellFormula
        {
            get { return _cell.GetFormula(); }
            set { _cell.SetFormula(value);  }
        }
        
        public string CellStyle
        {
            get { return _cell.GetStyle(); }
            set { _cell.SetStyle(value); }
        }

        public GenericCell(ICell cell)
        {
            _cell = cell; 
        }

        public GenericCell(SpreadsheetFactory factory)
        {
            _cell = factory.CreateCell(); 
        }
    }
}

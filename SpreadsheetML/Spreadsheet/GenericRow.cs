﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SageATL.Office.Spreadsheet.Generic
{
    public class GenericRow
    {
        private IRow _row; 
        public List<GenericCell> Cells
        {
            get {
                List<GenericCell> view = new List<GenericCell>();
                foreach (ICell c in _row.GetCells())
                {
                    view.Add(new GenericCell(c));
                }
                return view;
            }
        } 
        public uint Index
        {
            get { return _row.GetIndex();  }
        }

        public Nullable<double> Height
        {
            get { return _row.GetRowHeight(); }
            set { _row.SetRowHeight(value); }
        }

        public GenericRow(IRow row)
        {
            _row = row;
        }

        public GenericRow(SpreadsheetFactory factory)
        {
            _row = factory.CreateRow(); 
        }

        public void AddCell(string column)
        {
            _row.AddCell(column);
        }

    }
}

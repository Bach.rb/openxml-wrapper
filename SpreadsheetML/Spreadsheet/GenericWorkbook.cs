﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SageATL.Office.Spreadsheet.Generic
{
    public class GenericWorkbook
    {
        private SpreadsheetFactory _factory; 
        private IWorkbook _workbook;

        public List<GenericWorksheet> Sheets
        {
            get
            {
                List<GenericWorksheet> view = new List<GenericWorksheet>(); 
                foreach(IWorksheet w in _workbook.GetSheets())
                {
                    view.Add(new GenericWorksheet(w)); 
                }
                return view; 
            }
        }

        public GenericWorkbook(SpreadsheetFactory factory)
        {
            _factory = factory; 
            _workbook = factory.CreateWorkbook();
        }

        public GenericWorkbook(IWorkbook workbook)
        {
            _workbook = workbook; 
        }

        public void Open(MemoryStream stream)
        {
            _workbook.Open(stream);
        }

        public void Save()
        {
            _workbook.Save();  
        }

        public void SaveAs(Stream stream)
        {
            _workbook.SaveAs(stream); 
        }

        public void Close()
        {
            _workbook.Close();
        }

        public void AppendSheet()
        {
            _workbook.AppendSheet(); 
        }

        public void DeleteSheet(uint index)
        {
            _workbook.DeleteSheet(index); 
        }

    }
}

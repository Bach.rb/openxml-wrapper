﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SageATL.Office.Spreadsheet.Generic
{
    public class GenericWorksheet
    {
        private IWorksheet _worksheet; 

        public List<GenericRow> Rows
        {
            get
            {
                List<GenericRow> view = new List<GenericRow>();
                foreach (IRow r in _worksheet.GetRows())
                {
                    view.Add(new GenericRow(r));
                }
                return view;
            }
        }

        public string Name
        {
            get
            {
                return _worksheet.GetName(); 
            }
        }

        public uint Index
        {
            get
            {
                return _worksheet.GetIndex();
            }
        }

        public List<IColumn> Columns
        {
            get { return _worksheet.GetColumns(); }
            set { _worksheet.SetColumns(value); }
        }

        public List<IImage> Images
        {
            get { return _worksheet.GetImages(); }
            set { _worksheet.SetImages(value); }
        }

        public IWorksheetSettings Settings
        {
            get { return _worksheet.GetWorksheetSettings(); }
            set { _worksheet.SetWorksheetSettings(value); }
        }

        public GenericWorksheet(SpreadsheetFactory factory)
        {
            _worksheet = factory.CreateWorksheet();
        }

        public GenericWorksheet(IWorksheet worksheet)
        {
            _worksheet = worksheet; 
        }

        public void InsertRow(uint index)
        {
            _worksheet.InsertRow(index);
        }

        public void Save()
        {
            _worksheet.Save(); 
        }

        public void DeleteRow(uint index)
        {
            _worksheet.DeleteRow(index);
        }


    }
}

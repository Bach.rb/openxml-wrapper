﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Wordprocessing;
using SageATL.Office.Spreadsheet.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SageATL.Office.Spreadsheet.OpenXml
{
    public class OpenXmlCell : ICell
    {
        private SharedStringTablePart _stringTable;
        private Cell _cell;
        public OpenXmlCellReference CellReference;

        #region Constructor 

        public OpenXmlCell(string column, uint rowIndex)
        {
            CellReference = new OpenXmlCellReference(column, rowIndex);
        }

        public OpenXmlCell(Cell c, SharedStringTablePart sst)
        {
            _stringTable = sst; 
            _cell = c;
            CellReference = new OpenXmlCellReference(_cell.CellReference);
        }

        #endregion 

        public int GetColumnAsIndex()
        {
            string columnName = GetColumnFromReference().ToUpperInvariant(); 
            
            int index = 0;
            for (int i = 0; i < columnName.Length; i++)
            {
                index *= 26;
                index += (columnName[i] - 'A' + 1);
            }

            return index;
        }


        public string GetFormula()
        {
            return _cell.CellFormula?.InnerText; 
        }

        public string GetReference()
        {
            return _cell.CellReference; 
        }


        public void SetReference(string reference)
        {
            _cell.CellReference = reference;
        }

        public string GetValue()
        {
            if (_cell.DataType != null && _cell.DataType.InnerText == "s" && Int32.TryParse(_cell.InnerText, out int index))
                return _stringTable.SharedStringTable.ElementAt(index).InnerText;
            else
                return null;
        }

        public void SetValue(string value)
        {
            int lastElementAddedIndex = AddValueToStringTable(value); 
            CellValue cv = new CellValue(lastElementAddedIndex.ToString());
            _cell.CellValue = cv;
            _cell.DataType = new EnumValue<CellValues>(CellValues.SharedString);
        }

        public void SetFormula(string formula)
        {
            if (formula == null) _cell.CellFormula = null; 

            _cell.CellFormula = new CellFormula(formula); 
        }

        private int AddValueToStringTable(string value)
        {
            _stringTable.SharedStringTable.AppendChild(new SharedStringItem(new DocumentFormat.OpenXml.Spreadsheet.Text(value)));
            _stringTable.SharedStringTable.Save();
            int lastElementAddedIndex = _stringTable.SharedStringTable.Elements<SharedStringItem>().Count() - 1;
            return lastElementAddedIndex; 
        }

        private string GetColumnFromReference()
        {
            return new Regex("^([A-Z]+)([0-9]+)$", RegexOptions.Compiled | RegexOptions.IgnoreCase).Match(_cell.CellReference).Groups[1].Value;
        }

        public string GetStyle()
        {
            return _cell.StyleIndex == null ? null : _cell.StyleIndex.ToString(); 
        }

        public void SetStyle(string styleIndex)
        {
            if(UInt32.TryParse(styleIndex, out uint result))
            _cell.StyleIndex = result; 
        }
    }
}

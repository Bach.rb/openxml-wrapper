﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SageATL.Office.Spreadsheet.OpenXml
{
    public class OpenXmlCellReference : IComparable<OpenXmlCellReference>
    {
        private uint _columnIndex; 
        private string _column;
        private uint _row;

        #region Properties 

        public string Column
        {
            get
            {
                return _column;
            }

            set
            {
                _column = value;
                _columnIndex = GetColumnAsIndex(value); 
            }
        }

        public uint Row
        {
            get
            {
                return _row;
            }

            set
            {
                _row = value;
            }
        }

        #endregion
        
        #region Constructor 

        public OpenXmlCellReference(string column, uint row)
        {
            Column = column;
            Row = row;
        }

        public OpenXmlCellReference(string cellReference)
        {
            SetFromReference(cellReference); 
        }

        #endregion 

        public override string ToString()
        {
            return Column + _row; 
        }

        public void Previous()
        {
            Column = GetIndexAsColumn(_columnIndex - 1);
        }

        public void Next()
        {
            Column = GetIndexAsColumn(_columnIndex + 1);
        }

        public int CompareTo(OpenXmlCellReference anotherReference)
        {
            if (this._columnIndex < anotherReference._columnIndex) return -1;
            else if (this._columnIndex > anotherReference._columnIndex) return 1;
            else return 0; 
        }

        #region Private Methods 
        private uint GetColumnAsIndex(string column)
        {
            uint index = 0;
            for (int i = 0; i < column.Length; i++)
            {
                index *= 26;
                index += (uint)(column[i] - 'A' + 1);
            }

            return index;
        }

        private string GetIndexAsColumn(uint index)
        {
            var sb = new StringBuilder();
            while (index > 0)
            {
                int mod = ((int)index - 1) % 26;
                char c = (char)('A' + mod);
                sb.Append(c);
                index = (index - 1) / 26;
            }

            String str = new String(sb.ToString().Reverse().ToArray());
            return str;
        }

        private void SetFromReference(string cellReference) 
        {
            Match m = new Regex("^([A-Z]+)([0-9]+)$", RegexOptions.Compiled | RegexOptions.IgnoreCase).Match(cellReference);
            
            if (!m.Success) throw new Exception("Wrong cell reference");
            Column = m.Groups[1].Value;
            Row = UInt32.Parse(m.Groups[2].Value); 
        }

        #endregion

        #region Operator overloading
        // TODO : Add RowIndex comparison 

        public override bool Equals(object o)
        {
            if ((o == null) || !this.GetType().Equals(o.GetType())) return false; 
            
            var item = o as OpenXmlCellReference; 
            return item._columnIndex == this._columnIndex;
        }

        public override int GetHashCode()
        {
            return this._column.GetHashCode() + (int)_row;
        }

        public static bool operator ==(OpenXmlCellReference x, OpenXmlCellReference y)
        {
            return x._columnIndex == y._columnIndex;
        }

        public static bool operator !=(OpenXmlCellReference x, OpenXmlCellReference y)
        {
            return x._columnIndex != y._columnIndex;
        }

        public static bool operator >=(OpenXmlCellReference x, OpenXmlCellReference y)
        {
            return x._columnIndex >= y._columnIndex;
        }

        public static bool operator <=(OpenXmlCellReference x, OpenXmlCellReference y)
        {
            return x._columnIndex <= y._columnIndex;
        }

        public static bool operator >(OpenXmlCellReference x, OpenXmlCellReference y)
        {
            return x._columnIndex > y._columnIndex;
        }

        public static bool operator <(OpenXmlCellReference x, OpenXmlCellReference y)
        {
            return x._columnIndex < y._columnIndex;
        }



        #endregion 


    }
}

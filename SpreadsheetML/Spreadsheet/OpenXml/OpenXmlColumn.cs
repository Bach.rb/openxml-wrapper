﻿using DocumentFormat.OpenXml.Spreadsheet;
using SageATL.Office.Spreadsheet.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SageATL.Office.Spreadsheet.OpenXml
{
    public class OpenXmlColumn : IColumn
    {
        private Column _column;

        public OpenXmlColumn(Column column)
        {
            Column = column;
        }

        public Column Column
        {
            get
            {
                return _column;
            }

            set
            {
                _column = value;
            }
        }
    }
}

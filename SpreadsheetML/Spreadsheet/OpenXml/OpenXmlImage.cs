﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;
using SageATL.Office.Spreadsheet.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SageATL.Office.Spreadsheet.OpenXml
{
    public class OpenXmlImage : IImage
    {
        private string _relId; 
        private OpenXmlElement _anchor; 
        private ImagePart _imagePart; 

        public OpenXmlImage(ImagePart ip, OpenXmlElement anchor)
        {
            _anchor = anchor;
            _imagePart = ip;
        }

        public ImagePart ImagePart { get => _imagePart; set => _imagePart = value; }
        public OpenXmlElement Anchor { get => _anchor; set => _anchor = value; }

        public ImagePartType GetImageType()
        {
            switch (ImagePart.ContentType)
            {
                case "image/jpeg":
                    return ImagePartType.Jpeg;
                case "image/gif":
                    return ImagePartType.Gif;
                case "image/png":
                    return ImagePartType.Png;
                case "image/tiff":
                    return ImagePartType.Tiff;
                default:
                    throw new Exception("Error : Unknow image format"); 
            }
        }
    }
}

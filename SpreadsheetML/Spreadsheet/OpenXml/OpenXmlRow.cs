﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using SageATL.Office.Spreadsheet.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SageATL.Office.Spreadsheet.OpenXml
{
    public class OpenXmlRow : IRow
    {
        private SharedStringTablePart _stringTable;
        private Row _row;
        public List<OpenXmlCell> Cells;

        #region Events 
        public delegate void RowIndexChanged(uint newIndex);
        public event RowIndexChanged OnRowIndexChanged;

        public delegate void CellAdded(string column);
        public event CellAdded OnCellAdded;
        #endregion 


        #region Constructor 

        public OpenXmlRow()
        {
            Cells = new List<OpenXmlCell>();
            OnRowIndexChanged += UpdateCellsReference;
            OnCellAdded += ShiftCellsRight;
        }

        public OpenXmlRow(Row row, SharedStringTablePart sst)
        {
            _row = row;
            _stringTable = sst;
            Cells = new List<OpenXmlCell>();
            OnRowIndexChanged += UpdateCellsReference;
            OnCellAdded += ShiftCellsRight; 
            LoadCells(); 
        }

        #endregion 

        public List<ICell> GetCells()
        {
            return Cells.Cast<ICell>().ToList(); 
        }

        public uint GetIndex()
        {
            return _row.RowIndex; 
        }

        public void SetIndex(uint index)
        {
            _row.RowIndex = index;
            OnRowIndexChanged(index); 
        }

        public void AddCell(string column)
        {
            OnCellAdded(column);
            AddCellInternal(new OpenXmlCellReference(column, _row.RowIndex)); 
        }

        public void AppendCell()
        {
            throw new NotImplementedException();
        }

        public void DeleteCell()
        {
            throw new NotImplementedException();
        }

        public Nullable<double> GetRowHeight()
        {
            return _row.Height;
        }

        public void SetRowHeight(Nullable<double> height)
        {
            _row.CustomHeight = height != null ? true : false;
            _row.Height = height;
        }

        #region private methods 

        private void LoadCells()
        {
            foreach (Cell c in _row.Descendants<Cell>())
            {
                Cells.Add(new OpenXmlCell(c, _stringTable)); 
            }
        }

        private void UpdateCellsReference(uint newIndex)
        {
            foreach(OpenXmlCell cell in Cells)
            {
                string reference = cell.GetReference();
                string updatedReference = new Regex("^([A-Z]+)([0-9]+)$", RegexOptions.Compiled | RegexOptions.IgnoreCase).Match(reference).Groups[1].Value + newIndex;
                cell.SetReference(updatedReference); 
            }
        }

        private void ShiftCellsRight(string column)
        {
            IEnumerable<OpenXmlCell> cellsAfter = Cells.Where(c => c.CellReference >= new OpenXmlCellReference(column, _row.RowIndex));
            foreach (OpenXmlCell cell in cellsAfter)
            {
                cell.CellReference.Next();
                cell.SetReference(cell.CellReference.ToString()); 
            }
        }

        private void AddCellInternal(OpenXmlCellReference reference)
        {
            Cell newCell = new Cell();
            newCell.CellReference = reference.Column + reference.Row;


            if (!Cells.Any() || Cells.Max(c => c.CellReference) < reference)
            {
                _row.AppendChild(newCell);
            }
            else
            {
                var cellAfter = _row.Elements<Cell>().First(c => new OpenXmlCellReference(c.CellReference) > reference);
                _row.InsertBefore(newCell, cellAfter);
            }
            
            Cells.Add(new OpenXmlCell(newCell, _stringTable)); 
        }


        #endregion
    }
}

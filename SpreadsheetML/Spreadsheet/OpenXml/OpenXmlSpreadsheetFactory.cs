﻿using SageATL.Office.Spreadsheet.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SageATL.Office.Spreadsheet.OpenXml
{
    public class OpenXmlSpreadsheetFactory : SpreadsheetFactory
    {
        public override ICell CreateCell()
        {
            throw new NotImplementedException();
        }

        public override IRow CreateRow()
        {
            throw new NotImplementedException();
        }

        public override IWorkbook CreateWorkbook()
        {
            return new OpenXmlWorkbook(); 
        }

        public override IWorksheet CreateWorksheet()
        {
            return new OpenXmlWorksheet(); 
        }
    }
}

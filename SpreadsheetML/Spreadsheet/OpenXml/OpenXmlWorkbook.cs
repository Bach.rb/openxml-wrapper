﻿using SageATL.Office.Spreadsheet.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.IO;

namespace SageATL.Office.Spreadsheet.OpenXml
{
    public class OpenXmlWorkbook : IWorkbook
    {
        private SpreadsheetDocument _workbook;
        public List<OpenXmlWorksheet> worksheets;

        #region Constructor 

        public OpenXmlWorkbook () {
            worksheets = new List<OpenXmlWorksheet>(); 
        }

        #endregion 

        public List<IWorksheet> GetSheets()
        {
            return worksheets.Cast<IWorksheet>().ToList(); 
        }

        public void Open(MemoryStream stream)
        {
            _workbook = SpreadsheetDocument.Open(stream, true);
            LoadWorksheets(); 
        }

        public void Close()
        {
            if (_workbook != null) _workbook.Close();
        }

        public void Save()
        {
            _workbook.WorkbookPart.Workbook.Save(); 
        }

        public void SaveAs(Stream stream)
        {
            _workbook.WorkbookPart.Workbook.Save(stream);
        }

        public void AddSheet(uint index)
        {
            throw new NotImplementedException();
        }

        public void AppendSheet()
        {
            WorksheetPart newWorksheetPart = _workbook.WorkbookPart.AddNewPart<WorksheetPart>();
            newWorksheetPart.Worksheet = new Worksheet(new SheetData());
            Sheets sheets = _workbook.WorkbookPart.Workbook.GetFirstChild<Sheets>();
            string relationshipId = _workbook.WorkbookPart.GetIdOfPart(newWorksheetPart);

            uint sheetId = 1;
            if (sheets.Elements<Sheet>().Count() > 0)
            {
                sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
            }

            // Give the new worksheet a name.
            string sheetName = "Sheet" + sheetId;

            // Append the new worksheet and associate it with the workbook.
            Sheet sheet = new Sheet() { Id = relationshipId, SheetId = sheetId, Name = sheetName };
            sheets.Append(sheet);

            SharedStringTablePart sst = _workbook.WorkbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();

            this.worksheets.Add(new OpenXmlWorksheet(sheet, newWorksheetPart, sst));
        }

        public void DeleteSheet(uint index)
        {
            OpenXmlWorksheet sheet = worksheets.First(s => s.GetIndex() == index); 
            if (sheet != null)
            {
                sheet.Delete(_workbook);
                worksheets.Remove(sheet);
                DeleteFormulaReferences(Convert.ToInt32(index));
            }
        }

        #region private methods 

        private void LoadWorksheets()
        {
            OpenXmlSpreadsheetFactory factory = new OpenXmlSpreadsheetFactory(); 
            var sheets = _workbook.WorkbookPart.Workbook.Descendants<Sheet>();
            foreach(Sheet s in sheets)
            {
                WorksheetPart wp = (WorksheetPart)_workbook.WorkbookPart.GetPartById(s.Id);
                SharedStringTablePart sst = _workbook.WorkbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
                OpenXmlWorksheet sheet = new OpenXmlWorksheet(s, wp, sst);
                worksheets.Add(sheet); 
            }
        }

        private void DeleteFormulaReferences(int sheetId)
        {
            CalculationChainPart calculationChainPart = _workbook.WorkbookPart.CalculationChainPart;
            if (calculationChainPart != null)
            {
                CalculationChain calculationChain = calculationChainPart.CalculationChain;
                var calculationCells = calculationChain.Elements<CalculationCell>().ToList();
                calculationCells.RemoveAll(c => c.SheetId == sheetId);
                if (!calculationCells.Any())
                {
                    _workbook.WorkbookPart.DeletePart(calculationChainPart);
                }
            }
        }


        #endregion


    }
}

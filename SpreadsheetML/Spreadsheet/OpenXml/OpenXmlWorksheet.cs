﻿using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using SageATL.Office.Spreadsheet.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlipFill = DocumentFormat.OpenXml.Drawing.Spreadsheet.BlipFill;
using Picture = DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture;

namespace SageATL.Office.Spreadsheet.OpenXml
{
    public class OpenXmlWorksheet : IWorksheet
    {
        private SharedStringTablePart _stringTable;
        private Sheet _sheet;
        private WorksheetPart _wp;
        private OpenXmlWorksheetSettings _settings; 
        public List<OpenXmlRow> Rows;
        public List<OpenXmlColumn> Columns;
        public List<OpenXmlImage> Images;

        #region Events 
        public delegate void RowAdded(uint rowIndex);
        public event RowAdded OnRowAdded;
        #endregion 

        #region Constructor 

        public OpenXmlWorksheet()
        {
            Rows = new List<OpenXmlRow>();
        }

        public OpenXmlWorksheet(Sheet s, WorksheetPart wp, SharedStringTablePart sst)
        {
            _sheet = s;
            _wp = wp;
            _stringTable = sst;
            OnRowAdded += ShiftRowsDown;
            Rows = new List<OpenXmlRow>();
            Columns = new List<OpenXmlColumn>();
            Images = new List<OpenXmlImage>();
            LoadRows();
            LoadColumns();
            LoadImages();
            LoadSettings(); 
        }

        #endregion 

        public uint GetIndex()
        {
            return UInt32.Parse(_sheet.SheetId);
        }

        public string GetName()
        {
            return _sheet.Name;
        }

        public List<IRow> GetRows()
        {
            return Rows.Cast<IRow>().OrderBy(r => r.GetIndex()).ToList();
        }

        public List<IImage> GetImages()
        {
            return Images.Cast<IImage>().ToList();
        }

        public void SetImages(List<IImage> images)
        {
            if (images != null && images.Any())
            {
                if (_wp.DrawingsPart == null)
                {
                    _wp.AddNewPart<DrawingsPart>();
                    _wp.DrawingsPart.WorksheetDrawing = new WorksheetDrawing();
                    string relId = _wp.GetIdOfPart(_wp.DrawingsPart);
                    _wp.Worksheet.Append(new Drawing() { Id = relId });
                }

                foreach (var img in images)
                {
                    OpenXmlImage castedImage = img as OpenXmlImage;
                    // Creates Image Part
                    var imgPart = _wp.DrawingsPart.AddImagePart(castedImage.GetImageType());
                    imgPart.FeedData(castedImage.ImagePart.GetStream());
                    string relId = _wp.DrawingsPart.CreateRelationshipToPart(imgPart);
                    
                    // Creates Anchor Part
                    var anchorNode = castedImage.Anchor.CloneNode(true);
                    var picture = (Picture)anchorNode.ChildElements.First(ce => ce is Picture);
                    picture.BlipFill.Blip.Embed.Value = relId;
                    _wp.DrawingsPart.WorksheetDrawing.Append(anchorNode); 
                    
                   Images.Add(new OpenXmlImage(imgPart, anchorNode));
                }

            }
        }


        public void InsertRow(uint index)
        {
            OnRowAdded(index);
            _wp.Worksheet.Save(); 
            AddRow(index);
        }


        public void AppendRow()
        {
            throw new NotImplementedException();
        }

        public void DeleteRow(uint index)
        {
            Row r1 = _wp.Worksheet.GetFirstChild<SheetData>().Descendants<Row>().FirstOrDefault(r => r.RowIndex == index);
            OpenXmlRow r2 = Rows.FirstOrDefault(r => r.GetIndex() == index);
            if (r1 != null && r2 != null)
            {
                r1.Remove();
                Rows.Remove(r2);
            }
        }

        public void Save()
        {
            _wp.Worksheet.Save();
        }

        public List<IColumn> GetColumns()
        {
            return Columns.Cast<IColumn>().ToList();
        }
        public void SetColumns(List<IColumn> columns)
        {
            Columns addedColumns = new Columns();

            foreach (IColumn c in columns)
            {
                OpenXmlColumn col = (OpenXmlColumn)c;
                Column addedColumn = new DocumentFormat.OpenXml.Spreadsheet.Column() { Min = col.Column.Min, Max = col.Column.Max, Width = col.Column.Width, CustomWidth = col.Column.CustomWidth };
                addedColumns.Append(addedColumn);
                Columns.Add(new OpenXmlColumn(addedColumn));
            }
            _wp.Worksheet.InsertAt(addedColumns, 0);
        }

        public IWorksheetSettings GetWorksheetSettings()
        {
            return _settings; 
        }

        public void SetWorksheetSettings(IWorksheetSettings value)
        {
            if (value != null)
            {
                var castedValue = value as OpenXmlWorksheetSettings; 
                if (_settings == null)
                {
                    _settings = new OpenXmlWorksheetSettings(); 
                }

                if (castedValue.PageMargins != null)
                {
                    var clonedNode = (PageMargins)castedValue.PageMargins.CloneNode(true);
                    _wp.Worksheet.AppendChild(clonedNode);
                    _settings.PageMargins = clonedNode; 
                }

                if (castedValue.PageSetup != null)
                {
                    var clonedNode = (PageSetup)castedValue.PageSetup.CloneNode(true);
                    _wp.Worksheet.AppendChild(clonedNode);
                    _settings.PageSetup = clonedNode;
                }
            }
        }

        #region private methods

        private void LoadRows()
        {
            IEnumerable<Row> sourceRows = _wp.Worksheet.GetFirstChild<SheetData>().Descendants<Row>();
            foreach (Row row in sourceRows)
            {
                Rows.Add(new OpenXmlRow(row, _stringTable));
            }
        }

        private void LoadColumns()
        {
            Columns addedColumns = new Columns();
            Columns sourceColumns = _wp.Worksheet.GetFirstChild<Columns>();
            if (sourceColumns != null)
            {
                foreach (Column c in sourceColumns)
                {
                    Column addedColumn = new DocumentFormat.OpenXml.Spreadsheet.Column() { Min = c.Min, Max = c.Max, Width = c.Width, CustomWidth = c.CustomWidth };
                    addedColumns.Append(addedColumn);
                    Columns.Add(new OpenXmlColumn(addedColumn));
                }
            }
        }

        private void LoadImages()
        {
            var dPart = _wp.DrawingsPart;
            if (dPart != null)
            {
                var imagesParts = dPart.Parts.Where(p => p.OpenXmlPart is ImagePart);
                var allAnchors = dPart.WorksheetDrawing.Elements<TwoCellAnchor>();
                foreach (var anchor in allAnchors)
                {
                    var picture = (Picture)anchor.ChildElements.FirstOrDefault(ce => ce is Picture);
                    var correspondingImgPart = imagesParts.FirstOrDefault(ip => ip.RelationshipId == picture.BlipFill.Blip.Embed.Value);
                    ImagePart pp = (ImagePart)correspondingImgPart.OpenXmlPart;
                    if (correspondingImgPart != null)
                    {
                        Images.Add(new OpenXmlImage(pp, anchor));
                    }
                }
            }
        }

        private void LoadSettings()
        {
            _settings = new OpenXmlWorksheetSettings(); 
            _settings.PageMargins = _wp.Worksheet.GetFirstChild<PageMargins>(); 
            _settings.PageSetup = _wp.Worksheet.GetFirstChild<PageSetup>();
        }

        private void ShiftRowsDown(uint index)
        {
            IEnumerable<OpenXmlRow> rowsAfter = Rows.Where(r => r.GetIndex() >= index);
            if (rowsAfter.Any())
            {
                foreach (OpenXmlRow row in rowsAfter)
                {
                    row.SetIndex(row.GetIndex() + 1);
                }
            }
        }

        private void AddRow(uint index)
        {
            SheetData sd = _wp.Worksheet.GetFirstChild<SheetData>();
            Row newRow = new Row() { RowIndex = index };
            
            if (!GetRows().Any() || GetRows().Max(r => r.GetIndex()) < index) 
            {
                sd.Append(newRow);
            }
            else
            {
                var rowAfter = sd.Elements<Row>().First(r => r.RowIndex > index);
                sd.InsertBefore(newRow, rowAfter); 
            }

            Rows.Add(new OpenXmlRow(newRow, _stringTable));
        }


        #endregion

        public void Delete(SpreadsheetDocument document)
        {
            _sheet.Remove();
            document.WorkbookPart.DeletePart(_wp);
        }

    }
}

﻿
using DocumentFormat.OpenXml.Spreadsheet;
using SageATL.Office.Spreadsheet.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SageATL.Office.Spreadsheet.OpenXml
{
    public class OpenXmlWorksheetSettings : IWorksheetSettings
    {
        private PageMargins _pageMargins;
        private PageSetup _pageSetup;

        public PageMargins PageMargins { get => _pageMargins; set => _pageMargins = value; }
        public PageSetup PageSetup { get => _pageSetup; set => _pageSetup = value; }
    }
}

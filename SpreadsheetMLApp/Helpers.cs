﻿using SageATL.Office.Spreadsheet.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SpreadsheetMLImplementation
{
    public class Helpers
    {
        public void DuplicateWorksheet(GenericWorkbook workbook)
        {
            workbook.AppendSheet();
            var currentSheet = workbook.Sheets[0];
            var newSheet = workbook.Sheets.Last();
            foreach (var existingRow in currentSheet.Rows)
            {
                newSheet.InsertRow(existingRow.Index);
                var newRow = newSheet.Rows.First(r => r.Index == existingRow.Index);
                foreach (var existingCell in existingRow.Cells)
                {
                    string column = Regex.Match(existingCell.CellReference, "[A-Za-z]+", RegexOptions.IgnoreCase).Value;
                    newRow.AddCell(column);
                    var newCell = newRow.Cells.First(c => c.CellReference == existingCell.CellReference);
                    newCell.CellValue = existingCell.CellValue;
                    newCell.CellStyle = existingCell.CellStyle;
                }
            }

            newSheet.Settings = currentSheet.Settings;
            newSheet.Images = currentSheet.Images;
            newSheet.Save();
        }

        public void TestRowInsertion(GenericWorkbook workbook)
        {
            workbook.Sheets[0].InsertRow(15);
            workbook.Sheets[0].Save(); 
        }

        public void TestCellInsertion(GenericWorkbook workbook)
        {
            workbook.Sheets[0].Rows[0].AddCell("O"); 
        }
    }
}

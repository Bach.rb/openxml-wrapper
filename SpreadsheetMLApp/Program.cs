﻿using SageATL.Office.Spreadsheet.Generic;
using SageATL.Office.Spreadsheet.OpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SpreadsheetMLImplementation
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = "..\\..\\wb.xlsx";
            byte[] f = File.ReadAllBytes(path);
            using (var stream = new MemoryStream())
            {
                stream.Write(f, 0, (int)f.Length);
                var factory = new OpenXmlSpreadsheetFactory();
                var workbook = new GenericWorkbook(factory);
                workbook.Open(stream);

                Helpers h = new Helpers();
                //h.DuplicateWorksheet(workbook); 
                //h.TestRowInsertion(workbook);
                h.TestCellInsertion(workbook);

                workbook.Save();
                workbook.Close();

                File.WriteAllBytes(path, stream.ToArray());
            }
        }
    }
}

﻿using NUnit.Framework;
using SageATL.Office.Spreadsheet.OpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SpreadsheetMLTest
{
    [TestFixture]
    public class IntegrationTestCell : IntegrationTestInitializer
    {
        [Test]
        public void Should_Get_The_Cell_Reference()
        {
            CreateSpreadsheetIntoStream(sheetCount: 1, rowCount: 1, cellCount: 3);
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            wb.Open(testStream);
            Assert.That(wb.GetSheets().First().GetRows().First().GetCells().First().GetReference(), Is.EqualTo("A1"));
        }

        [Test]
        public void Should_Get_The_Cell_Value()
        {
            CreateSpreadsheetIntoStream(sheetCount: 1, rowCount: 3, cellCount: 3);
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            wb.Open(testStream);
            Assert.That(wb.GetSheets().First().GetRows().First().GetCells().First().GetValue(), Is.EqualTo("VALUE_A"));
        }

        [Test]
        public void Should_Get_The_Cell_Formula()
        {
            CreateSpreadsheetIntoStream(sheetCount: 1, rowCount: 3, cellCount: 3);
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            wb.Open(testStream);
            OpenXmlCell cell = wb.GetSheets().First().GetRows().First().GetCells().Cast<OpenXmlCell>().First();
            Assert.That(cell.GetFormula(), Is.EqualTo(null));
        }

        [Test]
        public void Should_Set_The_Cell_Value()
        {
            CreateSpreadsheetIntoStream(sheetCount: 1, rowCount: 3, cellCount: 3);
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            wb.Open(testStream);
            OpenXmlCell cell = wb.GetSheets().First().GetRows().First().GetCells().Cast<OpenXmlCell>().First();
            Assert.That(cell.GetValue(), Is.EqualTo("VALUE_A"));
            cell.SetValue("NEW_VALUE");
            Assert.That(cell.GetValue(), Is.EqualTo("NEW_VALUE"));
        }

        [Test]
        public void Should_Set_The_Cell_Formula()
        {
            CreateSpreadsheetIntoStream(sheetCount: 1, rowCount: 3, cellCount: 3);
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            wb.Open(testStream);
            OpenXmlCell cell = wb.GetSheets().First().GetRows().First().GetCells().Cast<OpenXmlCell>().First();
            Assert.That(cell.GetFormula(), Is.EqualTo(null));
            cell.SetFormula("=2+2");
            Assert.That(cell.GetFormula(), Is.EqualTo("=2+2"));
        }

        //[Test]
        //public void Should_Set_The_Formula_Style()
        //{
        //    Assert.Fail("Not done yet!"); 
        //}

        //[Test]
        //public void Should_Get_The_Formula_Style()
        //{
        //    Assert.Fail("Not done yet!");
        //}

    }
}

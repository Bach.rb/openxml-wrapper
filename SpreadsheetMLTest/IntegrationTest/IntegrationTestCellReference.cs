﻿using NUnit.Framework;
using SageATL.Office.Spreadsheet.OpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SpreadsheetMLTest
{
    [TestFixture]
    public class IntegrationTestCellReference : IntegrationTestInitializer
    {
        [Test]
        public void Should_Compare_Cell_Reference()
        {
            var reference = new OpenXmlCellReference("B1");
            var referenceLowerThan = new OpenXmlCellReference("A1");
            var referenceEqualTo = new OpenXmlCellReference("B1");
            var referenceGreaterThan = new OpenXmlCellReference("C1");

            Assert.That(reference.CompareTo(referenceLowerThan), Is.EqualTo(1));
            Assert.That(reference.CompareTo(referenceEqualTo), Is.EqualTo(0));
            Assert.That(reference.CompareTo(referenceGreaterThan), Is.EqualTo(-1));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using System.IO;
using System.Globalization;

namespace SpreadsheetMLTest
{
    [TestFixture]
    public class IntegrationTestInitializer
    {
        protected MemoryStream testStream;

        [SetUp]
        public void Init()
        {
            testStream = new MemoryStream();
        }

        [TearDown]
        public void End()
        {
            if (testStream != null)
            {
                testStream.Close();
            }
        }

        protected void CreateSpreadsheetIntoStream(int sheetCount = 1, int rowCount = 0, int cellCount = 0)
        {
            // Create a spreadsheet document by supplying the filepath.
            // By default, AutoSave = true, Editable = true, and Type = xlsx.
            SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.
                Create(testStream, SpreadsheetDocumentType.Workbook);

            // Add a WorkbookPart to the document.
            WorkbookPart workbookpart = spreadsheetDocument.AddWorkbookPart();
            workbookpart.Workbook = new Workbook();

            // Add string table 
            SharedStringTablePart shareStringPart = spreadsheetDocument.WorkbookPart.AddNewPart<SharedStringTablePart>();
            shareStringPart.SharedStringTable = new SharedStringTable();

            for (int i = 1; i <= sheetCount; i++)
            {
                // Ad sheet data and rows 
                SheetData sd = new SheetData();

                for (int j = 1; j <= rowCount; j++)
                {
                    Row r = new Row() { RowIndex = Convert.ToUInt32(j) };
                    int charCode = 65; // A
                    for (int k = 0; k < cellCount; k++)
                    {
                        shareStringPart.SharedStringTable.AppendChild(new SharedStringItem(new DocumentFormat.OpenXml.Spreadsheet.Text("VALUE_" + ((char)(charCode + k)))));
                        int count = shareStringPart.SharedStringTable.Count() - 1;
                        Cell c = new Cell() { CellReference = ((char)(charCode + k)).ToString() + j, CellValue = new CellValue(count.ToString()), DataType = new EnumValue<CellValues>(CellValues.SharedString) };
                        shareStringPart.SharedStringTable.AppendChild(new SharedStringItem(new DocumentFormat.OpenXml.Spreadsheet.Text("Value" + ((char)(charCode + k)))));
                        r.AppendChild(c);
                    }
                    sd.InsertAt(r, j - 1);
                }

                // Add a WorksheetPart to the WorkbookPart.
                WorksheetPart worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet(sd);

                // Add Sheets to the Workbook.
                Sheets sheets = spreadsheetDocument.WorkbookPart.Workbook.
                    AppendChild<Sheets>(new Sheets());

                // Append a new worksheet and associate it with the workbook.
                Sheet sheet = new Sheet()
                {
                    Id = spreadsheetDocument.WorkbookPart.
                    GetIdOfPart(worksheetPart),
                    SheetId = Convert.ToUInt32(i),
                    Name = "Sheet" + i
                };
                sheets.Append(sheet);
            }

            workbookpart.Workbook.Save();

            // Close the document.
            spreadsheetDocument.Close();
        }
    }
}

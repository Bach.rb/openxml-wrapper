﻿using NUnit.Framework;
using SageATL.Office.Spreadsheet.OpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SpreadsheetMLTest
{
    [TestFixture]
    public class IntegrationTestRow : IntegrationTestInitializer
    {
        [Test]
        public void Should_Get_The_Row_Index()
        {
            CreateSpreadsheetIntoStream(sheetCount: 1, rowCount: 1);
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            wb.Open(testStream);
            Assert.That(wb.GetSheets().First().GetRows().First().GetIndex(), Is.EqualTo(1));
        }

        [Test]
        public void Should_Set_The_Row_Index()
        {
            CreateSpreadsheetIntoStream(sheetCount: 1, rowCount: 1);
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            wb.Open(testStream);
            wb.GetSheets().First().GetRows().Cast<OpenXmlRow>().First().SetIndex(5);
            Assert.That(wb.GetSheets().First().GetRows().First().GetIndex(), Is.EqualTo(5));
        }

        [Test]
        public void Should_Get_The_Cells()
        {
            CreateSpreadsheetIntoStream(sheetCount: 1, rowCount: 1, cellCount: 5);
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            wb.Open(testStream);
            Assert.That(wb.GetSheets().First().GetRows().First().GetCells().ToList, Has.Count.EqualTo(5));
        }

        [Test]
        public void When_Row_Index_Is_Changed_Cells_References_Should_Update()
        {
            CreateSpreadsheetIntoStream(sheetCount: 1, rowCount: 1, cellCount: 3);
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            wb.Open(testStream);
            OpenXmlRow r = wb.GetSheets().First().GetRows().Cast<OpenXmlRow>().First();
            Assert.That(r.GetCells().Select(c => c.GetReference()).ToList(), Does.Contain("A1").And.Contain("B1").And.Contain("C1").And.Count.EqualTo(3));
            r.SetIndex(5);
            Assert.That(r.GetCells().Select(c => c.GetReference()).ToList(), Does.Contain("A5").And.Contain("B5").And.Contain("C5").And.Count.EqualTo(3));
        }

        [Test]
        public void When_Cell_Is_Inserted_It_Should_Be_At_The_Right_Position()
        {
            CreateSpreadsheetIntoStream(sheetCount: 1, rowCount: 1, cellCount: 3);
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            wb.Open(testStream);
            OpenXmlRow row = wb.GetSheets().First().GetRows().Cast<OpenXmlRow>().First();
            var cells = row.GetCells().Cast<OpenXmlCell>().ToList();
            Assert.That(cells.Select(c => c.GetValue()).ToList(), Has.Count.EqualTo(3).And.Contain("VALUE_A").And.Contain("VALUE_B").And.Contain("VALUE_C"));
            Assert.That(cells.First(c => c.GetReference() == "B1").GetValue(), Is.EqualTo("VALUE_B"));
            row.AddCell("B");
            cells = row.GetCells().Cast<OpenXmlCell>().ToList();
            Assert.That(cells.Select(c => c.GetValue()).ToList(), Has.Count.EqualTo(4).And.Contain("VALUE_A").And.Contain("VALUE_B").And.Contain("VALUE_C").And.Contain(null));
            //Assert.That(cells.First(c => c.GetReference() == "B1").GetValue(), Is.EqualTo(""));
        }

    }
}

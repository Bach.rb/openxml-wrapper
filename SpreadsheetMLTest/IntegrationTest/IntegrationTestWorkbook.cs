﻿using System;
using NUnit.Framework;
//using SageATL.Entity.ExcelReports.Entities;
using System.IO;
//using Excel = Microsoft.Office.Interop.Excel;
using System.Linq;
using System.Text;
using SageATL;
using SageATL.Office.Spreadsheet.OpenXml;
using SageATL.Office.Spreadsheet.Generic;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using SpreadsheetMLTest;

namespace SpreadsheetMLTest
{
    [TestFixture]
    public class IntegrationTestWorkbook : IntegrationTestInitializer
    {
        [Test]
        public void When_Valid_Document_Stream_Workbook_Should_Open()
        {
            CreateSpreadsheetIntoStream(sheetCount: 1);
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            wb.Open(testStream);
            Assert.IsTrue(wb.GetSheets().Any());
        }

        [Test]
        public void When_Null_Stream_Workbook_Opening_Sould_Fail()
        {
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            Assert.Throws(typeof(ArgumentNullException), () => {
                wb.Open(null);
            });
        }

        [Test]
        public void When_Saved_The_Stream_Should_Have_The_Data()
        {
            CreateSpreadsheetIntoStream(sheetCount: 1);
            OpenXmlWorkbook wb1 = new OpenXmlWorkbook();
            wb1.Open(testStream);
            Assert.That(wb1.GetSheets().Count(), Is.EqualTo(1));
            wb1.AppendSheet();
            wb1.Save();
            wb1.Close();
            OpenXmlWorkbook wb2 = new OpenXmlWorkbook();
            wb2.Open(testStream);
            Assert.That(wb2.GetSheets().Count(), Is.EqualTo(2));
        }

        //[Test]
        //public void When_Closed_The_Stream_Should_Have_Been_Disposed()
        //{
        //    Assert.Fail("Not done yet"); 
        //}

        [Test]
        public void When_New_Worksheet_Is_Appended_It_Should_Be_Added_At_The_End()
        {
            CreateSpreadsheetIntoStream(sheetCount: 1, rowCount: 1);
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            wb.Open(testStream);
            Assert.That(wb.GetSheets().OrderByDescending(s => s.GetIndex()).First().GetRows().Count(), Is.EqualTo(1));
            Assert.That(wb.GetSheets().Count(), Is.EqualTo(1));
            wb.AppendSheet();
            Assert.That(wb.GetSheets().OrderByDescending(s => s.GetIndex()).First().GetRows().Count(), Is.EqualTo(0));
            Assert.That(wb.GetSheets().Count(), Is.EqualTo(2));
        }

        [Test]
        public void When_Worksheet_Is_Deleted_It_Should_No_Longer_Be_In_The_Workbook()
        {
            CreateSpreadsheetIntoStream(sheetCount: 3);
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            wb.Open(testStream);
            Assert.That(wb.GetSheets().Count(), Is.EqualTo(3));
            Assert.That(wb.GetSheets().OrderBy(s => s.GetIndex()).First().GetIndex(), Is.EqualTo(1));
            wb.DeleteSheet(1);
            Assert.That(wb.GetSheets().Count(), Is.EqualTo(2));
            Assert.That(wb.GetSheets().OrderBy(s => s.GetIndex()).First().GetIndex(), Is.EqualTo(2));
        }


    }
}

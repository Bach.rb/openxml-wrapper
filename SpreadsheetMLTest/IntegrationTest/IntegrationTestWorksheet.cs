﻿using NUnit.Framework;
using SageATL.Office.Spreadsheet.OpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpreadsheetMLTest
{
    [TestFixture]
    public class IntegrationTestWorksheet : IntegrationTestInitializer
    {
        [Test]
        public void Should_Get_The_Index_Of_The_Sheet()
        {
            CreateSpreadsheetIntoStream(sheetCount: 1, rowCount: 1);
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            wb.Open(testStream);
            OpenXmlWorksheet sheet = wb.GetSheets().Cast<OpenXmlWorksheet>().First();
            Assert.That(sheet.GetIndex(), Is.EqualTo(1));
        }

        [Test]
        public void Should_Get_The_Name_Of_The_Sheet()
        {
            CreateSpreadsheetIntoStream(sheetCount: 1, rowCount: 1);
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            wb.Open(testStream);
            OpenXmlWorksheet sheet = wb.GetSheets().Cast<OpenXmlWorksheet>().First();
            Assert.That(sheet.GetName(), Is.EqualTo("Sheet1"));
        }

        [Test]
        public void Should_Get_The_Rows()
        {
            CreateSpreadsheetIntoStream(sheetCount: 1, rowCount: 3);
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            wb.Open(testStream);
            OpenXmlWorksheet sheet = wb.GetSheets().Cast<OpenXmlWorksheet>().First();
            Assert.That(sheet.Rows.Count(), Is.EqualTo(3));
        }

        [Test]
        public void When_Row_Is_Inserted_It_Should_Be_At_The_Right_Index()
        {
            CreateSpreadsheetIntoStream(sheetCount: 1, rowCount: 2, cellCount: 2);
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            wb.Open(testStream);
            OpenXmlWorksheet sheet = wb.GetSheets().Cast<OpenXmlWorksheet>().First();
            Assert.That(sheet.Rows.Where(r => r.GetIndex() == 1).First().GetCells().Count(), Is.EqualTo(2));
            sheet.InsertRow(1);
            Assert.That(sheet.Rows.Where(r => r.GetIndex() == 1).First().GetCells().Count(), Is.EqualTo(0));
            Assert.That(sheet.Rows.Count(), Is.EqualTo(3));
        }

        [Test]
        public void When_Row_Is_Deleted_It_Should_Be_Removed_From_The_Sheet()
        {
            CreateSpreadsheetIntoStream(sheetCount: 1, rowCount: 2);
            OpenXmlWorkbook wb = new OpenXmlWorkbook();
            wb.Open(testStream);
            OpenXmlWorksheet sheet = wb.GetSheets().Cast<OpenXmlWorksheet>().FirstOrDefault();
            sheet.DeleteRow(1);
            Assert.That(sheet.Rows.Count(), Is.EqualTo(1));
        }

    }
}

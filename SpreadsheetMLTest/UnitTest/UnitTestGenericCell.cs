﻿using SageATL.Office.Spreadsheet.Generic;
using Moq;
using SageATL.Office.Spreadsheet.OpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using NUnit;
using NUnit.Framework;

namespace SpreadsheetMLTest.UnitTest
{
    [TestFixture]
    public class UnitTestGenericCell
    {
        [Test]
        public void Should_Get_The_Cell_Reference()
        {
            var mock = new Mock<ICell>();
            mock.Setup(c => c.GetReference()).Returns("B5");
            GenericCell cell = new GenericCell(mock.Object);
            Assert.That(cell.CellReference, Is.EqualTo("B5"));
        }

        [Test]
        public void Should_Get_The_Cell_Value()
        {
            var mock = new Mock<ICell>();
            mock.Setup(c => c.GetValue()).Returns("VALUE");
            GenericCell cell = new GenericCell(mock.Object);
            Assert.That(cell.CellValue, Is.EqualTo("VALUE"));
        }

        [Test]
        public void Should_Set_The_Cell_Value()
        {
            var mock = new Mock<ICell>();
            mock.Setup(c => c.SetValue(It.IsAny<string>()));
            GenericCell cell = new GenericCell(mock.Object);
            cell.CellValue = "VALUE";
            mock.Verify(c => c.SetValue(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void Should_Get_The_Cell_Formula()
        {
            var mock = new Mock<ICell>();
            mock.Setup(c => c.GetFormula()).Returns("=2+2");
            GenericCell cell = new GenericCell(mock.Object);
            Assert.That(cell.CellFormula, Is.EqualTo("=2+2"));
        }

        [Test]
        public void Should_Set_The_Cell_Formula()
        {
            var mock = new Mock<ICell>();
            mock.Setup(c => c.SetFormula(It.IsAny<string>()));
            GenericCell cell = new GenericCell(mock.Object);
            cell.CellFormula = "=2+2";
            mock.Verify(c => c.SetFormula(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void Should_Get_The_Cell_Style()
        {
            var mock = new Mock<ICell>();
            mock.Setup(c => c.GetStyle()).Returns("style");
            GenericCell cell = new GenericCell(mock.Object);
            Assert.That(cell.CellStyle, Is.EqualTo("style"));
        }

        [Test]
        public void Should_Set_The_Cell_Style()
        {
            var mock = new Mock<ICell>();
            mock.Setup(c => c.SetStyle(It.IsAny<string>()));
            GenericCell cell = new GenericCell(mock.Object);
            cell.CellStyle = "style";
            mock.Verify(c => c.SetStyle(It.IsAny<string>()), Times.Once());
        }

    }
}

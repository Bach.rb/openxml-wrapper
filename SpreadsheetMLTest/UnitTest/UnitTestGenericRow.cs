﻿using SageATL.Office.Spreadsheet.Generic;
using Moq;
using SageATL.Office.Spreadsheet.OpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using NUnit;
using NUnit.Framework;

namespace SpreadsheetMLTest.UnitTest
{
    [TestFixture]
    public class UnitTestGenericRow
    {
        [Test]
        public void Should_Get_The_Row_Index()
        {
            var mock = new Mock<IRow>();
            mock.Setup(r => r.GetIndex()).Returns(3);
            GenericRow row = new GenericRow(mock.Object);
            Assert.That(row.Index, Is.EqualTo(3));
        }

        [Test]
        public void Should_Get_The_Cells()
        {
            var mock = new Mock<IRow>();
            var mockCell = new Mock<ICell>(); 
            mock.Setup(r => r.GetCells()).Returns(new List<ICell>() { mockCell.Object });
            GenericRow row = new GenericRow(mock.Object);
            Assert.That(row.Cells, Has.Count.EqualTo(1));
        }

        [Test]
        public void When_Cell_Added_It_Should_Add_The_Cell()
        {
            var mock = new Mock<IRow>();
            mock.Setup(r => r.AddCell(It.IsAny<string>()));
            GenericRow row = new GenericRow(mock.Object);
            row.AddCell("A"); 
            mock.Verify(r => r.AddCell(It.IsAny<string>()), Times.Once());
        }

       
    }
}

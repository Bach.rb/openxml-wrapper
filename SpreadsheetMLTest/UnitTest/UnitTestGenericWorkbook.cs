﻿using SageATL.Office.Spreadsheet.Generic;
using Moq; 
using SageATL.Office.Spreadsheet.OpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using NUnit;
using NUnit.Framework;

namespace SpreadsheetMLTest.UnitTest
{
    [TestFixture]
    public class UnitTestGenericWorkbook
    {
        [Test]
        public void When_Opening_From_Stream_It_Should_Call_The_Open_Method()
        {
            var mock = new Mock<IWorkbook>();
            mock.Setup(w => w.Open(It.IsNotNull<MemoryStream>()));
            GenericWorkbook wb = new GenericWorkbook(mock.Object);
            wb.Open(new MemoryStream());
            mock.Verify(w => w.Open(It.IsNotNull<MemoryStream>()), Times.Once()); 
        }

        [Test]
        public void When_Saved_It_Should_Call_The_Save_Method()
        {
            var mock = new Mock<IWorkbook>();
            mock.Setup(w => w.Save());
            GenericWorkbook wb = new GenericWorkbook(mock.Object);
            wb.Save(); 
            mock.Verify(w => w.Save(), Times.Once());
        }

        [Test]
        public void When_Sheet_Is_Appended_It_Should_Call_The_Append_Method()
        {
            var mock = new Mock<IWorkbook>();
            mock.Setup(w => w.AppendSheet());
            GenericWorkbook wb = new GenericWorkbook(mock.Object);
            wb.AppendSheet(); 
            mock.Verify(w => w.AppendSheet(), Times.Once());
        }

        [Test]
        public void When_Sheet_Is_Deleted_It_Should_Call_The_Delete_Method()
        {
            var mock = new Mock<IWorkbook>();
            mock.Setup(w => w.DeleteSheet(It.IsAny<uint>()));
            GenericWorkbook wb = new GenericWorkbook(mock.Object);
            wb.DeleteSheet(1);
            mock.Verify(w => w.DeleteSheet(It.IsAny<uint>()), Times.Once());
        }

        [Test]
        public void Should_Get_The_Sheets() 
        {
            var mock = new Mock<IWorkbook>();
            mock.Setup(w => w.GetSheets()).Returns(new List<IWorksheet>() { new OpenXmlWorksheet() });
            GenericWorkbook wb = new GenericWorkbook(mock.Object);
            Assert.That(wb.Sheets, Has.Count.EqualTo(1)); 
            mock.Verify(w => w.GetSheets(), Times.Once());
        }
    }
}

﻿using SageATL.Office.Spreadsheet.Generic;
using Moq;
using SageATL.Office.Spreadsheet.OpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using NUnit;
using NUnit.Framework;

namespace SpreadsheetMLTest.UnitTest
{
    [TestFixture]
    public class UnitTestGenericWorksheet
    {
        [Test]
        public void Should_Get_The_Sheet_Name()
        {
            var mock = new Mock<IWorksheet>();
            mock.Setup(w => w.GetName()).Returns("SheetName");
            GenericWorksheet ws = new GenericWorksheet(mock.Object);
            Assert.That(ws.Name, Is.EqualTo("SheetName"));
        }

        [Test]
        public void Should_Get_The_Sheet_Index()
        {
            var mock = new Mock<IWorksheet>();
            mock.Setup(w => w.GetIndex()).Returns(5);
            GenericWorksheet ws = new GenericWorksheet(mock.Object);
            Assert.That(ws.Index, Is.EqualTo(5));
        }

        [Test]
        public void Should_Get_The_Rows()
        {
            var mockSheet = new Mock<IWorksheet>();
            var mockRow = new Mock<IRow>(); 
            mockSheet.Setup(w => w.GetRows()).Returns(new List<IRow>() { mockRow.Object });
            GenericWorksheet ws = new GenericWorksheet(mockSheet.Object);
            Assert.That(ws.Rows, Has.Count.EqualTo(1));
        }

        [Test]
        public void Should_Get_The_Columns()
        {
            var mockSheet = new Mock<IWorksheet>();
            var mockColumn = new Mock<IColumn>(); 
            mockSheet.Setup(w => w.GetColumns()).Returns(new List<IColumn>() { mockColumn.Object });
            GenericWorksheet ws = new GenericWorksheet(mockSheet.Object);
            Assert.That(ws.Columns, Has.Count.EqualTo(1));
        }

        [Test]
        public void When_Row_Inserted_It_Shoud_Call_The_Insert_Method()
        {
            var mockSheet = new Mock<IWorksheet>();
            mockSheet.Setup(w => w.InsertRow(It.IsAny<uint>()));
            GenericWorksheet ws = new GenericWorksheet(mockSheet.Object);
            ws.InsertRow(1);
            mockSheet.Verify(w => w.InsertRow(It.IsAny<uint>()), Times.Once());
        }

        [Test]
        public void When_Row_Deleted_It_Shoud_Call_The_Delete_Method()
        {
            var mockSheet = new Mock<IWorksheet>();
            mockSheet.Setup(w => w.DeleteRow(It.IsAny<uint>()));
            GenericWorksheet ws = new GenericWorksheet(mockSheet.Object);
            ws.DeleteRow(1);
            mockSheet.Verify(w => w.DeleteRow(It.IsAny<uint>()), Times.Once());
        }
    }
}
